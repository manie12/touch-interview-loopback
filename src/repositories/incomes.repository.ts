import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {PostgresTrackerDataSource} from '../datasources';
import {Incomes, IncomesRelations} from '../models';

export class IncomesRepository extends DefaultCrudRepository<
  Incomes,
  typeof Incomes.prototype.id,
  IncomesRelations
> {
  constructor(
    @inject('datasources.postgresTracker') dataSource: PostgresTrackerDataSource,
  ) {
    super(Incomes, dataSource);
  }
}
