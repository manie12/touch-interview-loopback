import { inject, lifeCycleObserver, LifeCycleObserver } from '@loopback/core';
import { juggler } from '@loopback/repository';

const config = {
  name: 'postgresTracker',
  connector: 'postgresql',
  url: '',
  host: 'localhost',
  port: 5430,
  user: 'user',
  password: 'manie12',
  database: 'trackerDb'
};

// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
@lifeCycleObserver('datasource')
export class PostgresTrackerDataSource extends juggler.DataSource
  implements LifeCycleObserver {
  static dataSourceName = 'postgresTracker';
  static readonly defaultConfig = config;

  constructor(
    @inject('datasources.config.postgresTracker', { optional: true })
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
